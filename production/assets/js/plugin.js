/* ----------------------------------------------------------------------------
URL: http://bitbucket.org/mortar/equalise
Author: Daniel Charman (daniel@blackmaze.com.au)
Created: 2014-05-24
---------------------------------------------------------------------------- */
(function( $ ) {
 
    $.fn.equalise = function(options) {

        var defaults = {
            debug: false
        };
     
        var settings = $.extend( {}, defaults, options );

        var groups = Array();
 
        this.each(function() {
            if(groups[$(this).data('equalise')] === undefined) groups[$(this).data('equalise')] = 0;
        });

        for(var group in groups) {
            this.filter('[data-equalise="' + group + '"]').each(function() {
                if(settings.debug) console.log($(this).height(), $(this).innerHeight(), $(this).outerHeight(), $(this));
                if($(this).height() > groups[group]) groups[group] = $(this).height();
            });

            $('[data-equalise="' + group + '"]').css({
                height:     groups[group] + 'px'
            });

            if(settings.debug) console.log('[data-equalise="' + group + '"]', groups[group] + 'px');            
        }
 
        return this;
    };
 
}( jQuery ));